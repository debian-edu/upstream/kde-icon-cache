DESTDIR =
sbindir = /usr/sbin
iconsdir = /usr/share/icons
INSTALL = install -D -m755
INSTALL_DATA = install -D -m644

all:
	gzip -9 < blank-square.svg > blank-square.svgz

install: all
	$(INSTALL) update-kde-icon-cache \
		$(DESTDIR)$(sbindir)/update-kde-icon-cache
	$(INSTALL_DATA) blank-square.svgz $(DESTDIR)$(iconsdir)/blank-square.svgz

clean:
	$(RM) blank-square.svgz
